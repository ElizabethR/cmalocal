<?php


function annuaire_artisans_add() {
	global $wpdb;
	global $annuaire_artisans_db_version;

	$table_name = $wpdb->prefix . 'artisan';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		name tinytext NOT NULL,
		text text NOT NULL,
		url varchar(55) NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'annuaire_artisans_db_version', $annuaire_artisans_db_version );
}
?>